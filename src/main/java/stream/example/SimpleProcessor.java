/**
 * 
 */
package stream.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Processor;
import stream.annotations.Parameter;

/**
 * <p>
 * An example class implementing a very simple processor.
 * </p>
 * 
 * @author Christian Bockermann &lt;christian.bockermann@udo.edu&gt;
 * 
 */
public class SimpleProcessor implements Processor {

	static Logger log = LoggerFactory.getLogger(SimpleProcessor.class);

	// the attribute to check
	String key = "x";

	// the threshold to check against the attribute value
	Double threshold = 0.0d;

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	@Parameter(description = "The name of the attribute to check against the threshold, default is 'x'.")
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the threshold
	 */
	public Double getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold
	 *            the threshold to set
	 */
	@Parameter(description = "The value of the threshold, default is 0.0")
	public void setThreshold(Double threshold) {
		this.threshold = threshold;
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		if (!input.containsKey(key)) {
			return input;
		}

		Double value = new Double("" + input.get(key));
		if (value > threshold) {
			input.put("@threshold", "exceeded");
		} else {
			input.put("@threshold", "ok");
		}

		return input;
	}
}