


all:	standalone


standalone:
	mvn -Dstreams.scope=compile assembly:assembly

clean:
	mvn -DskipTests=true clean
